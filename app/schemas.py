from pydantic import BaseModel, EmailStr
# from pydantic.types import conint
from typing import Optional


class BaseUser(BaseModel):
    email: EmailStr
    password: str


class CreateUser(BaseUser):
    pass


class UserResponse(BaseModel):
    id: int
    email: EmailStr

    class Config:
        orm_mode = True


class Post(BaseModel):
    title: str
    content: str
    is_published: bool = True
    owner_id: int
    author: UserResponse

    class Config:
        orm_mode = True


class CreatePost(BaseModel):
    title: str
    content: str
    is_published: bool = True
    owner_id: int
    author: UserResponse


class PostResponse(BaseModel):
    Post: Post
    votes: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    user_id: int
    token: str
    token_type: str


class TokenData(BaseModel):
    id: Optional[str] = None


class Vote(BaseModel):
    post_id: int
    # dir: conint(le=1)
