from sqlalchemy.orm import Session
from fastapi import status, APIRouter, Depends, HTTPException

from .. import schemas, models, utils, oauth2
from ..database import get_db

router = APIRouter(
    prefix="/users",
    tags=["Users"]
)


# Create User
@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.UserResponse
)
def create_user(
        get_user: schemas.CreateUser,
        db: Session = Depends(get_db)
        # user: int = Depends(oauth2.get_current_user)
):
    # print(user.id)
    hashed_password = utils.hash(get_user.password)
    get_user.password = hashed_password
    user = models.User(**get_user.dict())
    db.add(user)
    db.commit()
    db.refresh(user)
    return user


# Get User By Id
@router.get("/{id}", response_model=schemas.UserResponse)
def get_user(
        id: int,
        db: Session = Depends(get_db),
        user: int = Depends(oauth2.get_current_user)
):
    user = db.query(models.User).filter(models.User.id == id).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="User Not Found",
        )
    return user
