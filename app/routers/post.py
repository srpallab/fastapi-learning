from fastapi import status, HTTPException, Depends, APIRouter, Response
from sqlalchemy import func
from sqlalchemy.orm import Session
from typing import List, Optional
from .. import schemas, models, oauth2
from ..database import get_db


router = APIRouter(
    prefix="/posts",
    tags=["Posts"]
)


# Read
@router.get("/", response_model=List[schemas.PostResponse])
def get_all_posts(
        db: Session = Depends(get_db),
        limit: int = 10,
        skip: int = 0,
        search: Optional[str] = ""
):
    print(limit)
    # posts = db.query(models.Post).filter(
    #     models.Post.title.contains(search)
    # ).limit(limit).offset(skip).all()
    posts = db.query(
        models.Post, func.count(models.Vote.post_id).label("votes")
    ).join(
        models.Vote, models.Vote.post_id == models.Post.id, isouter=True
    ).group_by(
        models.Post.id
    ).filter(
        models.Post.title.contains(search)
    ).limit(limit).offset(skip).all()

    return posts


# Read One Post
@router.get("/{id}", response_model=schemas.PostResponse)
def get_single_post(
        id,
        db: Session = Depends(get_db)
):
    # single_post = get_post_by_id(int(id))
    single_post = db.query(
        models.Post, func.count(models.Vote.post_id).label("votes")
    ).join(
        models.Vote, models.Vote.post_id == models.Post.id, isouter=True
    ).group_by(
        models.Post.id
    ).filter(models.Post.id == id).first()
    if not single_post:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No Post Found",
        )
    return single_post


# Create Post
@router.post(
    "/",
    status_code=status.HTTP_201_CREATED,
    response_model=schemas.PostResponse
)
def create_post(
        get_post: schemas.CreatePost,
        db: Session = Depends(get_db),
        user: int = Depends(oauth2.get_current_user)
):
    post = models.Post(owner_id=user.id, **get_post.dict())
    db.add(post)
    db.commit()
    db.refresh(post)
    return post


# Update
@router.put("/{id}", response_model=schemas.PostResponse)
def update_post(
        id: int,
        updated_post: schemas.CreatePost,
        db: Session = Depends(get_db),
        user: int = Depends(oauth2.get_current_user)
):
    post_query = db.query(models.Post).filter(models.Post.id == id)
    post = post_query.first()
    if post is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No Post Found"
        )
    if user.id != post.owner_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not authorize to perform this operation"
        )
    post_query.update(updated_post.dict(), synchronize_session=False)
    db.commit()
    return post_query.first()


# Delete
@router.delete("/{id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_post(
        id: int,
        db: Session = Depends(get_db),
        user: int = Depends(oauth2.get_current_user)
):
    post_query = db.query(models.Post).filter(models.Post.id == id)
    post = post_query.first()
    if post is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="No Post Found"
        )
    if user.id != post.owner_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="You are not authorize to perform this operation"
        )

    post_query.delete(synchronize_session=False)
    db.commit()
    return Response(status_code=status.HTTP_204_NO_CONTENT)
